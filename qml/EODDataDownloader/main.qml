// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
//import com.nokia.symbian 1.1

Rectangle {
    width: childrenRect.width + checkBoxSpacing * 2 + borderWidthChecks
    height: childrenRect.height + checkBoxSpacing * 2 + borderWidthChecks

    property int borderWidthChecks: 2
    property int checkBoxSpacing: 10

    Column {

        anchors.left: bhav.left
        anchors.right: startDate.left
        anchors.rightMargin: 20

        anchors.verticalCenter: startDate.verticalCenter

        spacing: 10

        ButtonIcon{
            id: startButton
            width: parent.width
            text: "Start"
            onClicked: {
                if(index.checked || bhav.checked)
                {
                    statusText.color = "green"
                    statusText.text = "Ready"

                    startButton.enabled = false
                    stopButton.enabled = true

                    //Disable all other options too
                    enableAll(false)

                    //Call the C++ function to start downloading the checked options
                    var downloadList = getDownloadList(false)
                    var stDate = new Date(dpStart.currentYear, dpStart.currentMonth, dpStart.currentDay)
                    var edDate = new Date(dpEnd.currentYear, dpEnd.currentMonth, dpEnd.currentDay)

                    eoddd.startDownload(downloadList,
                                        stDate,
                                        edDate)

                    //console.log(dpStart.currentYear +" "+ (dpStart.currentMonth + 1) +" "+ dpStart.currentDay)
                    //console.log(stDate.getFullYear() + " " + (stDate.getMonth() + 1) +" " + stDate.getDate()+"\n")

                    //console.log(dpEnd.currentYear +" "+ (dpEnd.currentMonth + 1) +" "+ dpEnd.currentDay)
                    //console.log(edDate.getFullYear() + " " + (edDate.getMonth() + 1) +" " + edDate.getDate())

                }
                else
                {
                    statusText.color = "red"
                    statusText.text = "Please enable BhavCopy and/or Index"
                }
            }
        }

        ButtonIcon {
            id: stopButton
            width: parent.width
            text: "Stop"
            enabled: false
            onClicked: {
                //Stop the downloading, it will stop from the next checkpoint; Maybe can abort in between too maybe ...

                statusText.color = "blue" //"turquoise"
                statusText.text = "Please wait for download to stop"

                eoddd.stopDownload()
            }
        }
    }
    Text {
        id: startDate

        anchors.right: datePickerStart.left
        anchors.rightMargin: 20
        anchors.verticalCenter: datePickerStart.verticalCenter
        text: "Start\nDate"
        color: enabled? "black": "gray"
    }

    Rectangle {
        id: datePickerStart
        anchors.top: parent.top
        anchors.topMargin: borderWidthChecks/2 + checkBoxSpacing
        anchors.left: bhavList.left


        radius: 6

        width: dpStart.width+borderWidthChecks + 2*checkBoxSpacing
        height: dpStart.height +borderWidthChecks + 2* checkBoxSpacing
        border.width: borderWidthChecks
        border.color: enabled? "black": "gray"


        DatePicker{
            id: dpStart
            anchors.top: datePickerStart.top
            anchors.left: datePickerStart.left
            anchors.leftMargin: borderWidthChecks/2 + checkBoxSpacing
            anchors.topMargin: borderWidthChecks/2 + checkBoxSpacing

        }
    }

    Text {
        id: endDate

        anchors.left: datePickerStart.right
        anchors.leftMargin: 30
        anchors.verticalCenter: datePickerStart.verticalCenter
        text: "End\nDate"
        color: enabled? "black": "gray"
    }

    Rectangle {
        id: datePickerEnd
        anchors.top: parent.top
        anchors.topMargin: borderWidthChecks/2 + checkBoxSpacing
        anchors.left: endDate.right
        anchors.leftMargin: 20


        radius: 6

        width: dpEnd.width+borderWidthChecks + 2*checkBoxSpacing
        height: dpEnd.height +borderWidthChecks + 2* checkBoxSpacing
        border.width: borderWidthChecks
        border.color: enabled? "black": "gray"

        DatePicker{
            id: dpEnd
            anchors.top: datePickerEnd.top
            anchors.left: datePickerEnd.left
            anchors.leftMargin: borderWidthChecks/2 + checkBoxSpacing
            anchors.topMargin: borderWidthChecks/2 + checkBoxSpacing
        }
    }

    CheckBox {
        id: bhav
        anchors.top: datePickerStart.bottom
        anchors.topMargin: 10 + borderWidthChecks + checkBoxSpacing
        anchors.left: parent.left
        anchors.leftMargin: 10

        text: "BhavCopy"
    }

    Rectangle {
        id: bhavList

        anchors.top: bhav.top
        anchors.topMargin: -borderWidthChecks/2 - checkBoxSpacing
        anchors.left: bhav.right
        anchors.leftMargin: 20

        radius: 6

        width: bhavRow.width+borderWidthChecks + 2*checkBoxSpacing
        height: bhavRow.height +borderWidthChecks + 2* checkBoxSpacing
        border.width: borderWidthChecks
        border.color: bhav.checked && bhav.enabled? "black": "gray"


        Row {
            id: bhavRow
            anchors.left: bhavList.left
            anchors.top: bhavList.top

            anchors.leftMargin: borderWidthChecks/2 + checkBoxSpacing
            anchors.topMargin: borderWidthChecks/2 + checkBoxSpacing

            spacing: checkBoxSpacing
            CheckBox{
                id: eq
                text: "Nifty EQ"
                enabled: bhav.checked && bhav.enabled
            }

            CheckBox{
                id: fut
                text: "Nifty Futures"
                enabled: bhav.checked && bhav.enabled
            }

            CheckBox{
                id: opt
                text: "Nifty Options"
                enabled: bhav.checked && bhav.enabled
            }

            CheckBox{
                id: cur
                text: "Currency"
                enabled: bhav.checked && bhav.enabled
            }

            CheckBox{
                id: mcx
                text: "MCX"
                enabled: bhav.checked && bhav.enabled
            }
        }
    }

    CheckBox {
        id: index
        anchors.top: bhavList.bottom
        anchors.topMargin: 20 + borderWidthChecks + checkBoxSpacing

        anchors.left: bhav.left

        text: "Index"
    }

    Rectangle {
        id: indexList

        anchors.top: index.top
        anchors.topMargin: -borderWidthChecks/2 - checkBoxSpacing

        anchors.left: bhav.right
        anchors.leftMargin: 20

        radius: 6

        width: indexGrid.width +borderWidthChecks + 2*checkBoxSpacing
        height: indexGrid.height +borderWidthChecks + 2*checkBoxSpacing
        border.width: borderWidthChecks
        border.color: index.checked && index.enabled? "black": "gray"

        Grid {
            id: indexGrid
            anchors.left: indexList.left
            anchors.top: indexList.top

            anchors.leftMargin: borderWidthChecks/2 + checkBoxSpacing
            anchors.topMargin: borderWidthChecks/2 + checkBoxSpacing

            spacing: 10
            CheckBox{
                id: nifty
                text: "Nifty"
                enabled: index.checked && index.enabled
            }

            CheckBox{
                id: banknifty
                text: "BankNifty"
                enabled: index.checked && index.enabled
            }

            CheckBox{
                id: vix
                text: "INDIAVIX"
                enabled: index.checked && index.enabled
            }

            CheckBox{
                id: niftyjunior
                text: "NIFTYJUNIOR"
                enabled: index.checked && index.enabled
            }

            CheckBox{
                id: nse100
                text: "NSE100"
                enabled: index.checked && index.enabled
            }

            CheckBox{
                id: midcap50
                text: "MIDCAP50"
                enabled: index.checked && index.enabled
            }

            CheckBox{
                id: nseit
                text: "NSEIT"
                enabled: index.checked && index.enabled
            }
        }
    }

    Rectangle {
        id: statusContainer

        anchors.top: indexList.bottom
        anchors.topMargin: checkBoxSpacing * 2

        anchors.left: index.left
        anchors.right: bhavList.right

        border.width: borderWidthChecks
        border.color: "black"

        height: 30

        Text {
            id: statusText
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 5
            color: "green"
            text: "Ready"
        }
    }

    Connections {
             target: eoddd
             onStatusUpdate: {
                 statusText.color = color
                 statusText.text = status
             }
             onDownloadStopped: {
                 stopCleanUp()
                 statusText.color = color
                 statusText.text = status
             }
         }


    Component.onCompleted: {
        var list = eoddd.loadSettings()
        updateChecks(list)
    }
    Component.onDestruction: {
        eoddd.saveSettings(getDownloadList(true))
    }

    function enableAll(enable)
    {
        startDate.enabled = datePickerStart.enabled = endDate.enabled = datePickerEnd.enabled = enable
        bhav.enabled = index.enabled = enable
    }

    function stopCleanUp()
    {
        stopButton.enabled = false
        startButton.enabled = true

        enableAll(true)
    }

    function updateChecks(list)
    {
        for(var i = 0; i < list.length; i++)
        {
            if(list[i] === "BHAV") bhav.checked = true;
            if(list[i] === "INDEX") index.checked = true;

            if(list[i] === "EQ") eq.checked = true
            if(list[i] === "FUT") fut.checked = true
            if(list[i] === "OPT") opt.checked = true
            if(list[i] === "CUR") cur.checked = true
            if(list[i] === "MCX") mcx.checked = true

            if(list[i] === "S&P CNX NIFTY") nifty.checked = true
            if(list[i] === "BANK NIFTY") banknifty.checked = true
            if(list[i] === "INDIAVIX") vix.checked = true
            if(list[i] === "CNX NIFTY JUNIOR") niftyjunior.checked = true
            if(list[i] === "CNX 100") nse100.checked = true
            if(list[i] === "NIFTY MID CAP 50") midcap50.checked = true
            if(list[i] === "CNX IT") nseit.checked = true
        }
    }

    function getDownloadList(save)
    {
        var downloadList = new Array()

        if(bhav.checked || save)
        {
            if(bhav.checked && save) downloadList.push("BHAV")

            if(eq.checked) downloadList.push("EQ")
            if(fut.checked) downloadList.push("FUT")
            if(opt.checked) downloadList.push("OPT")
            if(cur.checked) downloadList.push("CUR")
            if(mcx.checked) downloadList.push("MCX")
        }

        if(index.checked || save)
        {
            if(index.checked && save) downloadList.push("INDEX")

            if(nifty.checked) downloadList.push("S&P CNX NIFTY")
            if(banknifty.checked) downloadList.push("BANK NIFTY")
            if(vix.checked) downloadList.push("INDIAVIX")
            if(niftyjunior.checked) downloadList.push("CNX NIFTY JUNIOR")
            if(nse100.checked) downloadList.push("CNX 100")
            if(midcap50.checked) downloadList.push("NIFTY MID CAP 50")
            if(nseit.checked) downloadList.push("CNX IT")
        }

        return downloadList
    }

    /*Rectangle {
        id: logContainer
        anchors.top: index.bottom
        anchors.topMargin: checkBoxSpacing * 2

        anchors.bottom: parent.bottom
        anchors.bottomMargin: checkBoxSpacing
        anchors.left: index.left
        anchors.right: bhavList.right

        border.width: borderWidthChecks
        border.color: "black"

        Flickable {
            id: logFlickable

            anchors.fill: parent
            anchors.margins: 5
            contentHeight: log.height
            clip: true

            Text {
                id: log
                width: parent.width
                text: "I
am
testing
this
as
a
a
a
a
a

w
w
s
as

d
sagt
g
er
y
rty
ytj
ty
jhg
"
            }
        }
    }*/
}

