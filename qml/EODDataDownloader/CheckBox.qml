// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: checkBox

    property int marginWidth: 5
    radius: 6
    //color: "black"
    //border.color: "white"
    border.width: 0
    height: img.height
    width: label.implicitWidth + img.implicitWidth + 2*marginWidth
    property string text

    property bool checked: false

    MouseArea {

        anchors.fill: parent
        onClicked: {
            checkBox.checked =  checked ? false : true
        }
    }

    Image {
        id: img
        opacity: checked ? 1.0 : 0.1
        smooth: true
        //width: 12
        height: 30
        source:  "images/check.png"
        //anchors.fill: parent
    }
    Text {
        id: label
        //color: "white"
        font.pointSize: 8
        anchors.left: img.right
        anchors.right: parent.right

        anchors.leftMargin: marginWidth
        anchors.rightMargin: marginWidth

        anchors.verticalCenter: parent.verticalCenter
        text: checkBox.text

        color: enabled? "black": "gray"
    }

}
