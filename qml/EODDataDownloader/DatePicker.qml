// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    property int minimumYear: (new Date()).getFullYear() - 20
    property int maximumYear: (new Date()).getFullYear() + 20
    property int currentYear: (new Date()).getFullYear()

    property int currentMonth: (new Date()).getMonth() //0-11

    property int currentDay: (new Date()).getDate() // 1-31
    property int numberOfDays: getNumberOfDays(currentYear, currentMonth+1)

    property int spacing: 10
    property int heightSpacing: 5
    height: year.height + heightSpacing + yearTumbler.height

    width: (year.width > yearTumbler.width? year.width: yearTumbler.width) + spacing + year.width + spacing + year.width

    Text {
        id: year

        anchors.top: parent.top
        text: "Year"
        color: enabled? "black": "gray"
    }

    Text {
        id: month
        anchors.top: year.top
        anchors.left: year.right
        anchors.leftMargin: spacing
        text: "Month"
        color: enabled? "black": "gray"
    }

    Text {
        id: day
        anchors.top: month.top
        anchors.left: month.right
        anchors.leftMargin: spacing
        text: "Day"
        color: enabled? "black": "gray"
    }

    Tumbler {
        id: yearTumbler

        anchors.top: year.bottom
        anchors.topMargin: heightSpacing
        anchors.horizontalCenter: year.horizontalCenter
        Component.onCompleted: {
            var yearList = new Array()
            for(var i = minimumYear; i <= maximumYear; i++)
            {
                yearList.push(i)
            }
            populate(yearList, currentYear - minimumYear)
        }

        onContentChanged: {
            //console.log("dp "+minimumYear+ " " +indexToCenter + " " +currentYear)
                currentYear = minimumYear + indexToCenter
                updateDate()
        }
    }

    Tumbler {
        id: monthTumbler

        anchors.top: yearTumbler.top
        anchors.horizontalCenter: month.horizontalCenter

        Component.onCompleted: {
            var monthList = new Array()
            for(var i = 1; i <= 12; i++)
            {
                if(i < 10)
                    monthList.push("0" + i)
                else
                    monthList.push(""+ i)
            }

            populate(monthList, currentMonth)
        }

        onContentChanged: {
                currentMonth = indexToCenter
            //console.log("Mon " + (currentMonth+1))
                updateDate()
        }
    }

    Tumbler {
        id: dayTumbler

        anchors.top: yearTumbler.top
        anchors.horizontalCenter: day.horizontalCenter

        Component.onCompleted: {
            var dayList = new Array()
            for(var i = 1; i <= numberOfDays; i++)
            {
                if(i < 10)
                    dayList.push("0" + i)
                else
                    dayList.push(i)
            }

            populate(dayList, currentDay-1)
        }

        onContentChanged: {
                currentDay = indexToCenter+1
            //console.log("Curre Day "+currentDay )
        }
    }

    function getNumberOfDays(year, month) {
        var d = new Date(year, month, 0);
        return d.getDate();
    }

    function updateDate() {
        //

        numberOfDays = getNumberOfDays(currentYear, currentMonth+1)
        if(currentDay > numberOfDays) currentDay = numberOfDays

        var dayList = new Array()
        for(var i = 1; i <= numberOfDays; i++)
        {
            if(i < 10)
                dayList.push("0" + i)
            else
                dayList.push(i)
        }

        dayTumbler.populate(dayList, currentDay-1)
    }

}
