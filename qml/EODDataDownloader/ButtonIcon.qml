// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: buttonId

    property alias source: icon.source
    property alias text: buttonText.text
    property alias fontSize: buttonText.font.pointSize
    property  bool  pressedState: false
    signal clicked

    width: icon.width > buttonText.width ? icon.width + 10: buttonText.width+10
    height: (text == "" || icon.height > buttonText.height ? icon.height + 10: buttonText.height+10)
    radius: 6
    smooth: true
    border.width: 2
    border.color: (enabled) ? "black" : "gray"

    gradient: Gradient {
             GradientStop { position: 0.0; color: mouseArea.pressed || pressedState ? "lightGray": "darkGray" }
             GradientStop { position: 0.5; color: "white" }
             GradientStop { position: 1.0; color: mouseArea.pressed || pressedState ? "lightGray": "darkGray" }
         }

    Image {
        id: icon
    }

    Text {
        id: buttonText
        color: (buttonId.enabled) ? "black" : "gray"
    }

    onWidthChanged: {
        buttonText.anchors.horizontalCenter=buttonId.horizontalCenter
        icon.anchors.horizontalCenter=buttonId.horizontalCenter
    }

    onHeightChanged: {
        buttonText.anchors.verticalCenter=buttonId.verticalCenter
        icon.anchors.verticalCenter=buttonId.verticalCenter
    }

    MouseArea {
             id: mouseArea
             anchors.fill: parent
             hoverEnabled: false
             onClicked: {
                 //console.log("Hmmm ButtonIcon")
                 buttonId.clicked()
             }
         }
}
