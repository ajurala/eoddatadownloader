// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    property int indexToCenter:-1
    property int componentLoaded: -1

    property int numberOfVisibleItems: 3
    property int itemHeight: 20
    property color highlightColor: "light blue"

    signal contentChanged

    height: numberOfVisibleItems * itemHeight
    width: tumblerView.width

    ListView {
        id: tumblerView

        width: contentItem.childrenRect.width+10
        height: parent.height

        model: tumblerModel
        delegate: tumblerDelegate

        interactive: enabled // this is needed as some issue in the QT/QML, freezes app when flickable is disabled

        clip: true
        highlight:  Rectangle{
            radius: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            color: enabled? highlightColor: "light gray"
            border.width: 5
            border.color: enabled? highlightColor: "light gray"
        }

        preferredHighlightBegin: Math.floor(numberOfVisibleItems / 2)* itemHeight
        preferredHighlightEnd: preferredHighlightBegin + itemHeight

        highlightRangeMode: ListView.StrictlyEnforceRange

        snapMode: ListView.SnapToItem

        Component.onCompleted: {
            if(indexToCenter >= 0)
            {
                tumblerView.positionViewAtIndex(indexToCenter, ListView.Center)
            }

        }

        onCurrentIndexChanged: {
            indexToCenter = currentIndex
            //console.log(currentIndex);
        }

        onMovementEnded: {
            contentChanged()
        }

    }

    ListModel {
        id: tumblerModel
    }

    Component {
        id: tumblerDelegate
        Item {
            id: delItem
            anchors.left: parent.left
            anchors.leftMargin: 5

            width: textContent.width
            height: itemHeight

            Text {
                id: textContent
                text: content
                color: enabled? "black": "gray"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    tumblerView.currentIndex = index
                    contentChanged()
                }
            }
        }
    }

    function populate(contentList, index)
    {
        tumblerModel.clear()

        for(var i = 0; i < contentList.length; i++)
            tumblerModel.append({"content":contentList[i]})

        indexToCenter = index

        tumblerView.currentIndex = index
    }

}
