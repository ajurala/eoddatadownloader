#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include "eoddatadownloader.h"
#include <QDeclarativeContext>
#include <QtNetwork/QNetworkProxyFactory>

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    EODDataDownloader eoddd;
    QmlApplicationViewer viewer;
    QDeclarativeContext *context = viewer.rootContext();
    context->setContextProperty("eoddd", &eoddd);

    QNetworkProxyFactory::setUseSystemConfiguration(true);

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/EODDataDownloader/main.qml"));
    viewer.showExpanded();

    return app->exec();
}
