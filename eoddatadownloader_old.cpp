#include "eoddatadownloader.h"
#include <QDebug>
#include <QDir>
#include <QSettings>
#include "Archive.h"

using namespace bugless;

EODDataDownloader::EODDataDownloader(QObject *parent) :
    QObject(parent)
{
    indexMap["NIFTY"] = "http://nseindia.com/content/indices/histdata/S&P%20CNX%20NIFTYsdate-edate.csv";
    indexMap["NIFTYJUNIOR"] = "http://nseindia.com/content/indices/histdata/CNX%20NIFTY%20JUNIORsdate-edate.csv";
    indexMap["NSE100"] = "http://nseindia.com/content/indices/histdata/CNX%20100sdate-edate.csv";
    indexMap["NSE500"] = "http://nseindia.com/content/indices/histdata/S&P%20CNX%20500sdate-edate.csv";
    indexMap["MIDCAP50"] = "http://nseindia.com/content/indices/histdata/NIFTY%20MIDCAP%2050sdate-edate.csv";
    indexMap["NSEMIDCAP"] = "http://nseindia.com/content/indices/histdata/CNX%20MIDCAPsdate-edate.csv";
    indexMap["BANKNIFTY"] = "http://nseindia.com/content/indices/histdata/BANK%20NIFTYsdate-edate.csv";
    indexMap["NSEIT"] = "http://nseindia.com/content/indices/histdata/CNX%20ITsdate-edate.csv";
    indexMap["INDIAVIX"] = "http://www.nseindia.com/content/vix/histdata/hist_india_vix_sdate_edate.csv";

    bhavMap["OPT"] = bhavMap["FUT"] = "http://www.nseindia.com/content/historical/DERIVATIVES/YYYY/MMM/foDDMMMYYYYbhav.csv.zip";
    bhavMap["EQ"] = "http://www.nseindia.com/content/historical/EQUITIES/YYYY/MMM/cmDDMMMYYYYbhav.csv.zip";

    downloadIndex = -1;
    download = false;
    downloadError = false;
    downloadState = DOWNLOAD_NONE;

    fo =  false;

    //Create the headers
    manager = new QNetworkAccessManager(this);

    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(requestFinished(QNetworkReply*)));

    request.setRawHeader("Host","www.nseindia.com");
    request.setRawHeader("User-Agent"," Mozilla/5.0 (X11; Linux i686; rv:2.0) Gecko/20100101 Firefox/4.0");
    request.setRawHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    request.setRawHeader("Accept-Language"," en-us,en;q=0.5");
    request.setRawHeader("Accept-Charset"," ISO-8859-1,utf-8;q=0.7,*;q=0.7");
    request.setRawHeader("Keep-Alive","115");
    request.setRawHeader("Connection"," keep-alive");

    QDir dir;

    dir.setPath("Equities");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Futures");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Options");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Index");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Currency");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    logFile.setFileName("log.txt");
    logFile.open(QIODevice::WriteOnly | QIODevice::Text);

    logOutput.setDevice(&logFile);
}

EODDataDownloader::~EODDataDownloader()
{
    delete manager;
    logOutput.flush();
    logFile.close();
}

void EODDataDownloader::startDownload(QVariantList list, QDate stDate, QDate edDate)
{
    for(int i = 0; i < list.size(); i++)
    {
        logOutput<<list[i].toString() + " ";
    }
    logOutput<<stDate.toString() + " ";
    logOutput<<edDate.toString();
    logOutput<<"\n";

    //Start Downloading, signal done later
    if(list.size() > 0)
    {
        downloadList = list;
        downloadIndex = 0;

        download = true;

        if(downloadList.contains("OPT") && downloadList.contains("FUT"))
        {
            fo = true;
            downloadList.removeAll("OPT");
        }

        downloadDate = stDate;
        startDate = stDate;
        endDate = edDate;
        downloadNext();
    }
    else
    {
        downloadStop(false, "Nothing to Download");
    }

}

void EODDataDownloader::stopDownload()
{
    download = false;
}


void EODDataDownloader::saveSettings(QVariantList list)
{
    QSettings settings("PumaSoft", "EODDataDownloader");

    settings.clear();
    for(int i = 0; i < list.size(); i++)
    {
        settings.setValue(list[i].toString(), "1");
    }
}

QVariantList EODDataDownloader::loadSettings()
{
    QSettings settings("PumaSoft", "EODDataDownloader");

    QStringList keys = settings.allKeys();
    QVariantList keyList;
    QListIterator<QString> i(keys);

    while (i.hasNext()) {
      keyList << i.next();
    }

    return keyList;
}

void EODDataDownloader::requestFinished(QNetworkReply * reply)
{
    bool previousDownloadError = true;
    //If User requested for abort then stop immediately
    if(!download)
    {
        downloadStop(!download);

        logOutput<<"User Aborted";
        logOutput<<"\n";
        logOutput.flush();

        return;
    }
    else if(reply->error() == QNetworkReply::ContentNotFoundError)
            //&& downloadState == DOWNLOAD_DATA_EXIST_TEST)
    {
        //Data does not exist ...
        //TODO-Log

        logOutput<< downloadList[downloadIndex].toString() + ": " + downloadDate.toString()
                  + " Data does not exist";
        logOutput<<"\n";

    }
    else if(reply->error() != QNetworkReply::NoError)
    {
        QString downloadBhav = downloadList[downloadIndex].toString();
        QString status = "Error in Downloading " + downloadBhav + " "
                + (indexMap.contains(downloadBhav) ? startDate.toString() +" - " + endDate.toString()
                                                   : downloadDate.toString());
        QString color = "red";

        emit statusUpdate(status, color);

        logOutput<<status <<"Error Code" << reply->error() <<"Error String" <<reply->errorString();
        logOutput<<"\n";

        downloadError = true;
        previousDownloadError = true;

        //TODO - Log
    }
    else
    {
        QString downloadBhav = downloadList[downloadIndex].toString();

        // Everything is fine, get data into iobuffer and send it to quazip to extract then convert
        if(indexMap.contains(downloadBhav))
        {
            readIndexData(downloadBhav, reply);
        }
        else
        {
            if(downloadBhav == "EQ")
            {
                readEqData(reply);
            }
            else
            {
                readFOData(downloadBhav, reply);
            }
        }
    }

    reply->deleteLater();

    //Start next download if it exists
    downloadNext(previousDownloadError);
}

void EODDataDownloader::downloadNext(bool previousDownloadError)
{
    QString url;
    QString downloadBhav = downloadList[downloadIndex].toString();

    //Checking before initiating the download
    if(!download)
    {
        downloadStop(!download);
        logOutput<<"User Aborted! downloadNext()";
        logOutput<<"\n";
        logOutput.flush();

        return;
    }

    if((previousDownloadError && downloadState == DOWNLOAD_DATA_EXIST_TEST)
    || (downloadState == DOWNLOAD_DATA) )
    {
        downloadState = DOWNLOAD_NONE;

        // Increase the index if end date has reached ...
        // or if it is an index that was downloaded

        if(downloadDate == endDate || indexMap.contains(downloadBhav))
        {
            downloadDate = startDate;
            downloadIndex++;
        }
        else downloadDate = downloadDate.addDays(1);

        //qDebug()<< "Here ... " << downloadIndex <<" " << downloadDate;
    }

    //Check whether anything is left to download

    if(downloadIndex >= downloadList.size()) {
        //TODO - LOG
        downloadStop();
        logOutput<<"\nStopping as download list complete";
        logOutput<<"\n";

        logOutput.flush();

        return;
    }


    /*// If it is fresh start then check whether existence needs to be checked

    if(downloadState == DOWNLOAD_NONE && bhavMap.contains())
    {
        downloadState == DOWNLOAD_DATA_EXIST_TEST;
        //download the existance first ...
    }

    // else then it exists and download it ...
*/

    //Request for download

    downloadBhav = downloadList[downloadIndex].toString();

    QString status = "Downloading "+downloadBhav+ " "
            + (indexMap.contains(downloadBhav) ? startDate.toString() +" - " + endDate.toString()
                                               : downloadDate.toString());
    QString color = "green";

    //Currently downloading directly for any kind of data ...

    //TODO-log

    downloadState = DOWNLOAD_DATA;
    if(indexMap.contains(downloadBhav))
    {
        url = indexMap[downloadBhav];
        QString sdate = startDate.toString("dd-MM-yyyy");
        QString edate = endDate.toString("dd-MM-yyyy");

        url.replace("sdate", sdate);
        url.replace("edate", edate);
    }
    else if(bhavMap.contains(downloadBhav))
    {
        url = bhavMap[downloadBhav];
        url.replace("YYYY", downloadDate.toString("yyyy"));
        url.replace("MMM", downloadDate.toString("MMM").toUpper());
        url.replace("DD", downloadDate.toString("dd"));
    }
    else
    {
        // This should never happen
        //Invalid selection, stop the Download and throw error for now?
        //TODO - Log
        downloadError = false;
        downloadStop(false, "Invalid Selection of Bhavcopy List");

        logOutput<<"Why this happened ? Invalid selection, stop the Download and throw error for now?";
        logOutput<<"\n";
        logOutput.flush();


        return;
    }
    logOutput<<"\n";
    logOutput<<"URL :: "+url;
    logOutput<<"\n";


    request.setUrl(QUrl(url));
    logOutput<< status;
    logOutput<<"\n";


    emit statusUpdate(status, color);
    manager->get(request);

    return;
}

void EODDataDownloader::downloadStop(bool userAbort, QString statusAppend)
{
    QString status = userAbort ? "User Aborted !!!" : "Download Complete.";
    QString color = userAbort? "orange": "green";
    //if error had happened earlier then a different status
    if(downloadError) {
        status += " There was error during downloading. Check Log";
        color = "red";
    }

    status += " "+ statusAppend;
    emit downloadStopped(status, color);

    //TODO - LOG
    downloadCleanup();
}

void EODDataDownloader::downloadCleanup()
{
    downloadError = false;
    download = false;
    downloadIndex = -1;
    downloadList.clear();
}


void EODDataDownloader::readIndexData(QString index, QNetworkReply *reply)
{
    QString data = reply->readLine();
    QFile file("Index/"+index+".txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&file);


    //Another readline before start, to skip the first line
    data = reply->readLine();
    while(!data.isEmpty())
    {
        out << index+",";

        QStringList dataSections = data.split(",", QString::SkipEmptyParts);
        QDate date = QDate::fromString(dataSections[0].remove("\"").trimmed(), "dd-MMM-yyyy");
        QString dateString = date.toString("yyyyMMdd");

        out <<  dateString + ",";
        out <<  dataSections[1].remove("\"").trimmed() + ",";
        out <<  dataSections[2].remove("\"").trimmed() + ",";
        out <<  dataSections[3].remove("\"").trimmed() + ",";
        out <<  dataSections[4].remove("\"").trimmed() + ",";
        out <<  dataSections[5].remove("\"").trimmed() + "\n";

        data = reply->readLine();
    }

    file.close();
}

void EODDataDownloader::readEqData(QNetworkReply *reply)
{
    Archive a(reply, Archive::ZIP);
    QIODevice *dev = a.begin().device();
    QString dateString = downloadDate.toString("yyyyMMdd");
    QFile file("Equities/"+dateString+".txt");

    dev->open(QIODevice::ReadOnly);
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&file);

    QString data = dev->readLine();
    data = dev->readLine(); // skip first line
    while(!data.isEmpty())
    {
        QStringList dataSections = data.split(",", QString::SkipEmptyParts);

        if(dataSections[1] == "EQ")
        {
            out << dataSections[0] +",";
            out << dateString + ",";
            out << dataSections[2] + ",";
            out << dataSections[3] + ",";
            out << dataSections[4] + ",";
            out << dataSections[5] + ",";
            out << dataSections[8] + "\n";
        }
        data = dev->readLine();
    }

    file.close();
    dev->close();
}

void EODDataDownloader::readFOData(QString FnO, QNetworkReply *reply)
{
    fut.clear();
    opt.clear();

    Archive a(reply, Archive::ZIP);
    QIODevice *dev = a.begin().device();

    dev->open(QIODevice::ReadOnly);

    QString data = dev->readLine();
    data = dev->readLine(); // skip first line
    while(!data.isEmpty())
    {
        QStringList dataSections = data.split(",", QString::SkipEmptyParts);

        //Add to the fut db if its a new one
        QString scripName = dataSections[1]
                + (dataSections[0].startsWith("OPT") && (fo || FnO == "OPT")
                   ? dataSections[3]+dataSections[4]
                   : "" );

        ScripInfo info;
        info.expiryDate = QDate::fromString(dataSections[2], "dd-MMM-yyyy");

        info.open = dataSections[5];
        info.high = dataSections[6];
        info.low = dataSections[7];
        info.close = dataSections[8];

        info.volume = dataSections[10];
        info.openInterest = dataSections[12];

        if(dataSections[0].startsWith("FUT")
                && FnO == "FUT"
                && !fut[scripName].contains(info))
        {
            // A New data add ...
            fut[scripName].append(info);
        }
        else if(dataSections[0].startsWith("OPT")
                && (fo || FnO == "OPT")
                && !opt[scripName].contains(info))
        {
            // A New data add ...
            opt[scripName].append(info);
        }

        data = dev->readLine();
    }

    dev->close();

    logOutput<<"Done FO reading";
    logOutput<<"\n";


    QString dateString = downloadDate.toString("yyyyMMdd");

    if(FnO == "FUT")
    {
        logOutput<<"Fut Save";
        logOutput<<"\n";

        QFile file("Futures/"+dateString+".txt");
        saveFO(fut, file);
    }
    if(fo || FnO == "OPT")
    {
        logOutput<<"Opt Save";
        logOutput<<"\n";

        QFile file("Options/"+dateString+".txt");
        saveFO(opt, file);
    }


}

void EODDataDownloader::saveFO(QMap<QString, QList<ScripInfo> > &fo, QFile &file)
{
    QString dateString = downloadDate.toString("yyyyMMdd");

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    QList<QString> scripNames = fo.keys();

    qDebug() << scripNames.size();
    for(int i = 0; i < scripNames.size(); i++)
    {
        QList<ScripInfo> scripDetailsList = fo[scripNames[i]];

        qSort(scripDetailsList);

        qDebug()<<scripDetailsList.size();
        for(int j = 0; j < scripDetailsList.size(); j++)
        {
            ScripInfo scrip = scripDetailsList[j];

            qDebug()<<scripNames[i] + "-" + QString().fill('I', j+1) + ",";

            out << scripNames[i] + "-" + QString().fill('I', j+1) + ",";
            out << dateString + ",";
            out << scrip.open + ",";
            out << scrip.high + ",";
            out << scrip.low + ",";
            out << scrip.close + ",";

            out << scrip.volume + ",";
            out << scrip.openInterest + "\n";
        }
    }

    file.close();
}


bool ScripInfo::operator== ( const ScripInfo & other ) const
{
    return other.expiryDate == this->expiryDate;
}

bool ScripInfo::operator< ( const ScripInfo & other ) const
{
    return this->expiryDate < other.expiryDate;
}
