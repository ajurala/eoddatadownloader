#ifndef EODDATADOWNLOADER_H
#define EODDATADOWNLOADER_H

#include <QObject>
#include <QVariant>
#include <QDate>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QMap>
#include <QFile>
#include <QTextStream>
#include <QWebPage>
#include <QWebFrame>
#include <QWebElement>

enum DownloadState {
    DOWNLOAD_NONE = 0,
    DOWNLOAD_DATA_EXIST_TEST,
    DOWNLOAD_DATA_EXIST_TEST_2,
    DOWNLOAD_DATA
};

class ScripInfo
{
public:
    QDate expiryDate;

    QString open;
    QString high;
    QString low;
    QString close;

    QString volume;
    QString openInterest;

    bool operator== ( const ScripInfo & other ) const;

    bool operator< ( const ScripInfo & other ) const;
};

class EODDataDownloader : public QObject
{
    Q_OBJECT
public:
    explicit EODDataDownloader(QObject *parent = 0);
    ~EODDataDownloader();

    Q_INVOKABLE void startDownload(QVariantList list, QDate startDate, QDate endDate);
    Q_INVOKABLE void stopDownload();
    Q_INVOKABLE void saveSettings(QVariantList list);
    Q_INVOKABLE QVariantList loadSettings();

signals:
    void statusUpdate(QString status, QString color);
    void downloadStopped(QString status, QString color);
    
public slots:
    void requestFinished(QNetworkReply * reply);
    void loadFinished(bool ok);
    void initiateMCXDownload();

private:

    void downloadStop(bool userAbort = false, QString statusAppend = "");
    void downloadCleanup();
    void downloadNext(bool previousDownloadError = false);

    void readIndexData(QString index, QNetworkReply *reply);    
    void readFOData(QString index, QNetworkReply *reply);
    void readEqData(QNetworkReply *reply);
    void readCurData(QNetworkReply *reply);
    void readMCXData(QNetworkReply * reply);

    void saveFO(QMap<QString, QList<ScripInfo> > &fo, QFile &file, bool option = false);

    QFile logFile;
    QTextStream logOutput;

    QFile consolidatedFile;
    QTextStream consolidatedOutput;

    QWebPage webPage;
    QMap<QString, QString> indexMap;
    QMap<QString, QString> bhavMap;
    QString alternateMCXurl;

    QMap<int, QString> seriesMap;

    QNetworkAccessManager *manager ;
    QNetworkRequest request;

    QVariantList downloadList;
    int downloadIndex;

    bool download;

    bool downloadError;

    DownloadState downloadState;
    QDate downloadDate;

    QDate startDate;
    QDate endDate;
    bool fo;

    QMap<QString, QList<ScripInfo> > fut;
    QMap<QString, QList<ScripInfo> > opt;
};

#endif // EODDATADOWNLOADER_H
