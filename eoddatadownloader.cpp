#include "eoddatadownloader.h"
#include <QDebug>
#include <QDir>
#include <QSettings>
#include <QTimer>
#include "Archive.h"
#include "QDbfTable.h"
#include "QDbfRecord.h"

using namespace bugless;
using namespace QDbf;

EODDataDownloader::EODDataDownloader(QObject *parent) :
    QObject(parent)
{
    indexMap["S&P CNX NIFTY"] = "http://nseindia.com/content/indices/histdata/CNX NIFTYsdate-edate.csv";
    indexMap["CNX NIFTY JUNIOR"] = "http://nseindia.com/content/indices/histdata/CNX NIFTY JUNIORsdate-edate.csv";
    indexMap["CNX 100"] = "http://nseindia.com/content/indices/histdata/CNX 100sdate-edate.csv";
    indexMap["CNX 500"] = "http://nseindia.com/content/indices/histdata/CNX 500sdate-edate.csv";
    indexMap["NIFTY MID CAP 50"] = "http://nseindia.com/content/indices/histdata/NIFTY MIDCAP 50sdate-edate.csv";
    indexMap["NSE MIDCAP"] = "http://nseindia.com/content/indices/histdata/CNX MIDCAPsdate-edate.csv";
    indexMap["BANK NIFTY"] = "http://nseindia.com/content/indices/histdata/CNX BANKsdate-edate.csv";
    indexMap["CNX IT"] = "http://nseindia.com/content/indices/histdata/CNX ITsdate-edate.csv";
    indexMap["INDIAVIX"] = "http://www.nseindia.com/content/vix/histdata/hist_india_vix_sdate_edate.csv";

    bhavMap["OPT"] = bhavMap["FUT"] = "http://www.nseindia.com/content/historical/DERIVATIVES/YYYY/MMM/foDDMMMYYYYbhav.csv.zip";
    bhavMap["EQ"] = "http://www.nseindia.com/content/historical/EQUITIES/YYYY/MMM/cmDDMMMYYYYbhav.csv.zip";
    bhavMap["CUR"] = "http://www.nseindia.com/archives/cd/bhav/CD_BhavcopyDDmmyy.zip";
    bhavMap["MCX"] = "http://www.mcxindia.com/SitePages/BhavCopyDateWise.aspx";
    alternateMCXurl = "http://www.mcxindia.com/SitePages/BhavCopyDateWiseArchive.aspx";

    seriesMap[1] = "I";     seriesMap[2] = "II";    seriesMap[3] = "III";
    seriesMap[4] = "IV";    seriesMap[5] = "V";     seriesMap[6] = "VI";
    seriesMap[7] = "VII";    seriesMap[8] = "VIII";    seriesMap[9] = "IX";
    seriesMap[10] = "X";    seriesMap[11] = "XI";    seriesMap[12] = "XII";
    seriesMap[13] = "XIII";    seriesMap[14] = "XIV";    seriesMap[15] = "XV";


    downloadIndex = -1;
    download = false;
    downloadError = false;
    downloadState = DOWNLOAD_NONE;

    fo =  false;

    webPage.setForwardUnsupportedContent(true);
    manager = webPage.networkAccessManager(); //new QNetworkAccessManager(this);

    connect(&webPage, SIGNAL(loadFinished(bool)),
            this, SLOT(loadFinished(bool)));

    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(requestFinished(QNetworkReply*)));

    //Create the headers
    request.setRawHeader("Host","www.nseindia.com");
    request.setRawHeader("User-Agent"," Mozilla/5.0 (X11; Linux i686; rv:2.0) Gecko/20100101 Firefox/4.0");
    request.setRawHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    request.setRawHeader("Accept-Language"," en-us,en;q=0.5");
    request.setRawHeader("Accept-Charset"," ISO-8859-1,utf-8;q=0.7,*;q=0.7");
    request.setRawHeader("Keep-Alive","115");
    request.setRawHeader("Connection"," keep-alive");
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QDir dir;

    dir.setPath("Equities");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Futures");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Options");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Index");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Currency");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("MCX");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    dir.setPath("Consolidated");
    if(!dir.exists()) QDir().mkdir(dir.dirName());

    int i = 0;
    QString currentDate = QDate::currentDate().toString("yyyyMMdd");
    QString fileName = currentDate;
    while(QFile::exists("Consolidated/"+fileName+".txt"))
    {
        i++;
        fileName = currentDate + "_" + QString::number(i);
    }

    QString curDir = QDir::currentPath();

    QDir::setCurrent("Consolidated");

    consolidatedFile.setFileName(fileName+".txt");
    consolidatedFile.open(QIODevice::WriteOnly | QIODevice::Text);

    consolidatedOutput.setDevice(&consolidatedFile);

    QDir::setCurrent(curDir);

    logFile.setFileName("log.txt");
    logFile.open(QIODevice::WriteOnly | QIODevice::Text);

    logOutput.setDevice(&logFile);
}

EODDataDownloader::~EODDataDownloader()
{
    delete manager;
    consolidatedFile.close();
    logOutput.flush();
    logFile.close();
}

void EODDataDownloader::startDownload(QVariantList list, QDate stDate, QDate edDate)
{
    for(int i = 0; i < list.size(); i++)
    {
        logOutput<<list[i].toString() + " ";
    }
    logOutput<<stDate.toString() + " ";
    logOutput<<edDate.toString();
    logOutput<<"\n";

    //Start Downloading, signal done later
    if(list.size() > 0)
    {
        downloadList = list;
        downloadIndex = 0;

        download = true;

        if(downloadList.contains("OPT") && downloadList.contains("FUT"))
        {
            fo = true;
            downloadList.removeAll("OPT");
        }

        downloadDate = stDate;
        startDate = stDate;
        endDate = edDate;
        downloadNext();
    }
    else
    {
        downloadStop(false, "Nothing to Download");
    }

}

void EODDataDownloader::stopDownload()
{
    download = false;
}


void EODDataDownloader::saveSettings(QVariantList list)
{
    QSettings settings("PumaSoft", "EODDataDownloader");

    settings.clear();
    for(int i = 0; i < list.size(); i++)
    {
        settings.setValue(list[i].toString(), "1");
    }
}

QVariantList EODDataDownloader::loadSettings()
{
    QSettings settings("PumaSoft", "EODDataDownloader");

    QStringList keys = settings.allKeys();
    QVariantList keyList;
    QListIterator<QString> i(keys);

    while (i.hasNext()) {
      keyList << i.next();
    }

    return keyList;
}

void EODDataDownloader::requestFinished(QNetworkReply * reply)
{
    bool previousDownloadError = false;
    QString downloadBhav = downloadList[downloadIndex].toString();
    //qDebug()<<"requestFinished"<<downloadBhav<<downloadState;

    //If User requested for abort then stop immediately
    if(!download)
    {
        downloadStop(!download);

        logOutput<<"User Aborted";
        logOutput<<"\n";
        logOutput.flush();

        return;
    }
    else if(reply->error() == QNetworkReply::ContentNotFoundError)
            //&& downloadState == DOWNLOAD_DATA_EXIST_TEST)
    {
        //Data does not exist ...
        //TODO-Log

        qDebug()<<reply->url();
        qDebug()<<"Encoded query list present "<<reply->url().hasQuery();
        qDebug()<<reply->url().encodedQuery();
        qDebug()<<reply->url().queryItems();

        logOutput<< downloadList[downloadIndex].toString() + ": " + downloadDate.toString()
                  + " Data does not exist";
        logOutput<<"\n";

    }
    else if(reply->error() != QNetworkReply::NoError)
    {
        QString downloadBhav = downloadList[downloadIndex].toString();
        QString status = "Error in Downloading " + downloadBhav + " "
                + (indexMap.contains(downloadBhav) ? startDate.toString() +" - " + endDate.toString()
                                                   : downloadDate.toString());
        QString color = "red";

        emit statusUpdate(status, color);

        logOutput<<status <<"\nError Code " << reply->error() <<"\nError String " <<reply->errorString();
        logOutput<<"\n";
        qDebug()<<status <<"\nError Code " << reply->error() <<"\nError String " <<reply->errorString();

        downloadError = true;
        previousDownloadError = true;

        //TODO - Log
    }
    else
    {

        // Everything is fine, get data into iobuffer and send it to quazip to extract then convert
        if(downloadState == DOWNLOAD_DATA)
        {
            if(indexMap.contains(downloadBhav))
            {
                readIndexData(downloadBhav, reply);
            }
            else
            {
                if(downloadBhav == "MCX")
                {
                    /*qDebug()<<"Got MCX data";
                    QByteArray data = reply->readAll();
                    qDebug()<<"File SIZE"<<data.size();
                    QStringList list = QString(data).split("\n");
                    for(int i = 0; i < list.count(); i++)
                    qDebug()<<list[i];*/
                    readMCXData(reply);

                }
                else if(downloadBhav == "EQ")
                {
                    readEqData(reply);
                }
                else if(downloadBhav == "CUR")
                {
                    readCurData(reply);
                }
                else
                {
                    readFOData(downloadBhav, reply);
                }
            }
        }
        else
        {
            if(downloadState == DOWNLOAD_DATA_EXIST_TEST_2)
            {
                QTimer::singleShot(1, this, SLOT(initiateMCXDownload()));
            }
            logOutput<< "Downloaded the Existance successfully\n";
        }
        //if exist test state, we are not worried about the data as such ... ignore it and read the next state
    }

    //reply->deleteLater();

    //Start next download if it exists
    if(previousDownloadError ||
       !(downloadBhav == "MCX" && (downloadState == DOWNLOAD_DATA_EXIST_TEST
                               || downloadState == DOWNLOAD_DATA_EXIST_TEST_2)))
        downloadNext(previousDownloadError);
}

void EODDataDownloader::initiateMCXDownload()
{
    QString html = webPage.mainFrame()->toHtml();

    if(html.count(downloadDate.toString("MM/dd/yyyy")) > 0)
    {
        downloadState = DOWNLOAD_DATA;
        if(!webPage.mainFrame()->findFirstElement("#btnLink_Excel").isNull())
            webPage.mainFrame()->evaluateJavaScript("this.__doPostBack('btnLink_Excel','');");
        else
            downloadNext(false);
    }
    else
    {
        QTimer::singleShot(1, this, SLOT(initiateMCXDownload()));
    }
}

void EODDataDownloader::loadFinished(bool ok)
{
    //qDebug()<<"LoadFinished"<<ui->webView->page()->bytesReceived();
    qDebug()<<"Load successful"<<ok;
    if(ok && downloadState == DOWNLOAD_DATA_EXIST_TEST)
    {
        QString url = alternateMCXurl;
        if(QDate::currentDate().year() != downloadDate.year()  && url != webPage.mainFrame()->url().toString())
        {
            qDebug()<<"Starting to MCX test -1";

            //url changes ...
            downloadState = DOWNLOAD_DATA_EXIST_TEST;
            webPage.mainFrame()->setUrl(QUrl(url));

            qDebug()<<url;
        }
        else
        {
            /*QString data = webPage.mainFrame()->toHtml();
            qDebug()<<"DATA SIZE"<<data.size();
            QStringList list = data.split("\n");
            for(int i = 0; i < list.count(); i++)
                qDebug()<<list[i];*/

            qDebug()<<"Starting to MCX test 2";

            downloadState = DOWNLOAD_DATA_EXIST_TEST_2;
            //change the date and click the button
            qDebug()<<webPage.mainFrame()->evaluateJavaScript("document.getElementById('mTbdate').value = '"+downloadDate.toString("MM/dd/yyyy")+"'");
            webPage.mainFrame()->evaluateJavaScript("document.getElementById('mImgBtnGo').click()");
        }
    }

}

void EODDataDownloader::downloadNext(bool previousDownloadError)
{
    QString url;
    QString downloadBhav = downloadList[downloadIndex].toString();

    qDebug()<<"DownloadNext";
    //Checking before initiating the download
    if(!download)
    {
        downloadStop(!download);
        logOutput<<"User Aborted! downloadNext()";
        logOutput<<"\n";
        logOutput.flush();

        return;
    }

    if((previousDownloadError && (downloadState == DOWNLOAD_DATA_EXIST_TEST
                                  || downloadState == DOWNLOAD_DATA_EXIST_TEST_2))
    || (downloadState == DOWNLOAD_DATA) )
    {
        downloadState = DOWNLOAD_NONE;

        // Increase the index if end date has reached ...
        // or if it is an index that was downloaded

        if(downloadDate == endDate || indexMap.contains(downloadBhav))
        {
            downloadDate = startDate;
            downloadIndex++;
        }
        else downloadDate = downloadDate.addDays(1);

        //qDebug()<< "Here ... " << downloadIndex <<" " << downloadDate;
    }

    //Check whether anything is left to download

    if(downloadIndex >= downloadList.size()) {
        //TODO - LOG
        downloadStop();
        logOutput<<"\nStopping as download list complete";
        logOutput<<"\n";

        logOutput.flush();
        consolidatedOutput.flush();

        return;
    }


    // If it is fresh start then check whether existence needs to be checked
    downloadBhav = downloadList[downloadIndex].toString();
    QString status = "Downloading "+downloadBhav+ " "
            + (indexMap.contains(downloadBhav) ? startDate.toString() +" - " + endDate.toString()
                                               : downloadDate.toString());
    QString color = "green";

    if(downloadState == DOWNLOAD_NONE && indexMap.contains(downloadBhav))
    {
        bool vixData = downloadBhav.contains("VIX");
        downloadState = DOWNLOAD_DATA_EXIST_TEST;
        //download the existance first ...

        QString fromDateString = startDate.toString("dd-MM-yyyy");
        QString toDateString = endDate.toString("dd-MM-yyyy");

        //QString postdata="indexType="+downloadBhav+"&fromDate="+fromDateString+"&toDate="+toDateString;

        QString urlString;
        if(vixData)
            urlString = "http://www.nseindia.com/marketinfo/vix/hist_vix_data.jsp?FromDate="+fromDateString+"&ToDate="+toDateString;
        else
            urlString = "http://www.nseindia.com/marketinfo/indices/histdata/historicalindices.jsp";


        QUrl url(urlString);

        if(!vixData)
        {
            url.addQueryItem("indexType", downloadBhav);
            url.addQueryItem("fromDate", fromDateString);
            url.addQueryItem("toDate", toDateString);
        }

        logOutput<<"\n";
        logOutput<<"URL :: "+urlString;
        if(!vixData)
        {
           logOutput<<"\nPostData :: "+url.encodedQuery();
        }
        logOutput<<"\n\n";

        request.setUrl(url);

        logOutput<< status;
        logOutput<<"\n";


        emit statusUpdate(status, color);
        if(vixData)
            manager->get(request);
        else
            manager->post(request, url.encodedQuery());

    }

    // else then it exists and download it ...
    else
    {

    //Request for download

    //Currently downloading directly for any kind of data ...

    //TODO-log

    qDebug()<<"Call to download for "<<downloadBhav;

    downloadState = DOWNLOAD_DATA;
    if(indexMap.contains(downloadBhav))
    {
        url = indexMap[downloadBhav];
        QString sdate = startDate.toString("dd-MM-yyyy");
        QString edate = endDate.toString("dd-MM-yyyy");

        url.replace("sdate", sdate);
        url.replace("edate", edate);
    }
    else if(bhavMap.contains(downloadBhav))
    {        
        url = bhavMap[downloadBhav];
        url.replace("YYYY", downloadDate.toString("yyyy"), Qt::CaseSensitive);
        url.replace("MMM", downloadDate.toString("MMM").toUpper(), Qt::CaseSensitive);

        //For Currency ...
        url.replace("yy", downloadDate.toString("yy"), Qt::CaseSensitive);
        url.replace("mm", downloadDate.toString("MM"), Qt::CaseSensitive);

        url.replace("DD", downloadDate.toString("dd"), Qt::CaseSensitive);
    }
    else
    {
        // This should never happen
        //Invalid selection, stop the Download and throw error for now?
        //TODO - Log
        downloadError = false;
        downloadStop(false, "Invalid Selection of Bhavcopy List");

        logOutput<<"Why this happened ? Invalid selection, stop the Download and throw error for now?";
        logOutput<<"\n";
        logOutput.flush();


        return;
    }
    logOutput<<"\n";
    logOutput<<"URL :: "+url;
    logOutput<<"\n";


    //if(downloadBhav.contains("MCX"))
    //    request.setRawHeader("Host","www.mcxindia.com");
    //else
    //    request.setRawHeader("Host","www.nseindia.com");

    request.setUrl(QUrl(url));

    logOutput<< status;
    logOutput<<"\n";


    emit statusUpdate(status, color);
    if(downloadBhav.contains("MCX"))
    {

        if(downloadDate == startDate
          || (downloadDate.year() == QDate::currentDate().year() && url != webPage.mainFrame()->url().toString()))
        {
            qDebug()<<"Starting to MCX test 0";

            downloadState = DOWNLOAD_DATA_EXIST_TEST;
            webPage.mainFrame()->setUrl(QUrl(url));

            qDebug()<<url;
        }
        else
        {
            qDebug()<<"Starting to MCX test 1";
            downloadState = DOWNLOAD_DATA_EXIST_TEST_2;
            //change the date and click the button
            webPage.mainFrame()->evaluateJavaScript("document.getElementById('mTbdate').value = '"+downloadDate.toString("MM/dd/yyyy")+"'");
            webPage.mainFrame()->evaluateJavaScript("document.getElementById('mImgBtnGo').click()");
        }
    }
    else
        manager->get(request);
    }
    return;
}

void EODDataDownloader::downloadStop(bool userAbort, QString statusAppend)
{
    QString status = userAbort ? "User Aborted !!!" : "Download Complete.";
    QString color = userAbort? "orange": "green";
    //if error had happened earlier then a different status
    if(downloadError) {
        status += " There was error during downloading. Check Log";
        color = "red";
    }

    status += " "+ statusAppend;
    emit downloadStopped(status, color);

    //TODO - LOG
    downloadCleanup();
}

void EODDataDownloader::downloadCleanup()
{
    downloadError = false;
    download = false;
    downloadIndex = -1;
    downloadList.clear();
}


void EODDataDownloader::readIndexData(QString index, QNetworkReply *reply)
{
    index.remove(" ");
    QString data = reply->readLine();
    QFile file("Index/"+index+".txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&file);


    //Another readline before start, to skip the first line
    data = reply->readLine();
    while(!data.isEmpty())
    {
        QStringList dataSections = data.split(",", QString::SkipEmptyParts);
        QDate date = QDate::fromString(dataSections[0].remove("\"").trimmed(), "dd-MMM-yyyy");
        QString dateString = date.toString("yyyyMMdd");

        QString scripData = index+","

                            + dateString + ","
                            + dataSections[1].remove("\"").trimmed() + ","
                            + dataSections[2].remove("\"").trimmed() + ","
                            + dataSections[3].remove("\"").trimmed() + ","
                            + dataSections[4].remove("\"").trimmed() + ","
                            + dataSections[5].remove("\"").trimmed() + "\n";

        out << scripData;
        consolidatedOutput << scripData;


        /*out << index+",";

        out <<  dateString + ",";
        out <<  dataSections[1].remove("\"").trimmed() + ",";
        out <<  dataSections[2].remove("\"").trimmed() + ",";
        out <<  dataSections[3].remove("\"").trimmed() + ",";
        out <<  dataSections[4].remove("\"").trimmed() + ",";
        out <<  dataSections[5].remove("\"").trimmed() + "\n";*/

        data = reply->readLine();
    }

    file.close();
}

void EODDataDownloader::readMCXData(QNetworkReply *reply)
{
    QString data = reply->readLine();
    QString dateString = downloadDate.toString("yyyyMMdd");

    QFile file("MCX/"+dateString+".txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    fut.clear();

    //Another readline before start, to skip the first line
    data = reply->readLine();
    data = data.trimmed();
    while(!data.isEmpty())
    {
        QStringList dataSections = data.split(",", QString::SkipEmptyParts);

        //Add to the fut db if its a new one
        QString scripName = dataSections[1];

        ScripInfo info;
        info.expiryDate = QDate::fromString(dataSections[2], "dd MMM yyyy");

        info.open = dataSections[3];
        info.high = dataSections[4];
        info.low = dataSections[5];
        info.close = dataSections[6];

        info.volume = dataSections[8];
        info.openInterest = dataSections[11];

        if(!fut[scripName].contains(info))
        {
            // A New data add ...
            fut[scripName].append(info);
        }

        data = reply->readLine();
        data = data.trimmed();
    }

    saveFO(fut, file);
    file.close();
}

void EODDataDownloader::readCurData(QNetworkReply *reply)
{
    Archive a(reply, Archive::ZIP);
    QString fileName =  "CD_NSE_FO"+downloadDate.toString("ddMMyy")+".dbf";
    if(a.files().contains(fileName))
    {
        QString filePath = QDir::tempPath()+"/"+fileName;
        a.extractFile(fileName, QDir::temp());
        //extract the dbase and save ...
        QDbfTable table(filePath);

        QString dateString = downloadDate.toString("yyyyMMdd");

        fut.clear();
        table.open();

        table.first();

//        if( table.size() > 0)
//        {
//            QDbfRecord record = table.record();
//            QString header;
//            for(int j = 0; j < record.count(); ++j)
//            {
//                header += record.fieldName(j)+",";
//            }
//            qDebug()<<header;
//        }

        for(int i = 0; i < table.size(); i++)
        {
            QDbfRecord record = table.record();
//            QString dataString;

            //for(int j = 0; j < record.count(); ++j)
//            {
//                    dataString += record.value(j).toString()+",";
//            }
            //qDebug()<<dataString;

            QString scripNameDetails = record.value(0).toString();
            if(scripNameDetails.startsWith("FUT"))
            {
            //Add to the fut db if its a new one
                const int namePosition = 6;
                const int nameLength = 6;
                QString scripName = scripNameDetails.mid(namePosition, nameLength);

                const int datePosition = 12;
                const int dateLength = 11;
                ScripInfo info;
                info.expiryDate = QDate::fromString(scripNameDetails.mid(datePosition, dateLength), "dd-MMM-yyyy");

                info.open = record.value(2).toString();
                info.high = record.value(3).toString();
                info.low = record.value(4).toString();
                info.close = record.value(5).toString();

                info.volume = record.value(9).toString();
                info.openInterest = record.value(8).toString();

                if(!fut[scripName].contains(info))
                {
                    // A New data add ...
                    fut[scripName].append(info);
                }
            }

            table.next();
        }

        QFile file("Currency/"+dateString+".txt");
        saveFO(fut, file);

        table.close();

        QFile::remove(filePath);
    }
}

void EODDataDownloader::readEqData(QNetworkReply *reply)
{
    Archive a(reply, Archive::ZIP);
    QIODevice *dev = a.begin().device();
    QString dateString = downloadDate.toString("yyyyMMdd");
    QFile file("Equities/"+dateString+".txt");

    dev->open(QIODevice::ReadOnly);
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream out(&file);

    QString data = dev->readLine();
    data = dev->readLine(); // skip first line
    while(!data.isEmpty())
    {
        QStringList dataSections = data.split(",", QString::SkipEmptyParts);

        if(dataSections[1] == "EQ" || dataSections[1] == "BE")
        {
            QString scripData = dataSections[0] +","
                                + dateString + ","
                                + dataSections[2] + ","
                                + dataSections[3] + ","
                                + dataSections[4] + ","
                                + dataSections[5] + ","
                                + dataSections[8] + "\n";

            out << scripData;
            consolidatedOutput << scripData;

            /*out << dataSections[0] +",";
            out << dateString + ",";
            out << dataSections[2] + ",";
            out << dataSections[3] + ",";
            out << dataSections[4] + ",";
            out << dataSections[5] + ",";
            out << dataSections[8] + "\n"; */
        }
        data = dev->readLine();
    }

    file.close();
    dev->close();
}

void EODDataDownloader::readFOData(QString FnO, QNetworkReply *reply)
{
    fut.clear();
    opt.clear();

    Archive a(reply, Archive::ZIP);
    QIODevice *dev = a.begin().device();

    dev->open(QIODevice::ReadOnly);

    QString data = dev->readLine();
    data = dev->readLine(); // skip first line
    while(!data.isEmpty())
    {
        QStringList dataSections = data.split(",", QString::SkipEmptyParts);

        //Add to the fut db if its a new one
        QString scripName = dataSections[1]
                + (dataSections[0].startsWith("OPT") && (fo || FnO == "OPT")
                   ? dataSections[3]+dataSections[4]
                   : "" );

        ScripInfo info;
        info.expiryDate = QDate::fromString(dataSections[2], "dd-MMM-yyyy");

        info.open = dataSections[5];
        info.high = dataSections[6];
        info.low = dataSections[7];
        info.close = dataSections[8];

        info.volume = dataSections[10];
        info.openInterest = dataSections[12];

        if(dataSections[0].startsWith("FUT")
                && FnO == "FUT"
                && !fut[scripName].contains(info))
        {
            // A New data add ...
            fut[scripName].append(info);
        }
        else if(dataSections[0].startsWith("OPT")
                && (fo || FnO == "OPT")
                && !opt[scripName].contains(info))
        {
            // A New data add ...
            opt[scripName].append(info);
        }

        data = dev->readLine();
    }

    dev->close();

    logOutput<<"Done FO reading";
    logOutput<<"\n";


    QString dateString = downloadDate.toString("yyyyMMdd");

    if(FnO == "FUT")
    {
        logOutput<<"Fut Save";
        logOutput<<"\n";

        QFile file("Futures/"+dateString+".txt");
        saveFO(fut, file);
    }
    if(fo || FnO == "OPT")
    {
        logOutput<<"Opt Save";
        logOutput<<"\n";

        QFile file("Options/"+dateString+".txt");
        saveFO(opt, file, true);
    }


}

void EODDataDownloader::saveFO(QMap<QString, QList<ScripInfo> > &fo, QFile &file, bool option)
{
    QString dateString = downloadDate.toString("yyyyMMdd");

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    QList<QString> scripNames = fo.keys();

    qDebug() << scripNames.size();
    for(int i = 0; i < scripNames.size(); i++)
    {
        QList<ScripInfo> scripDetailsList = fo[scripNames[i]];

        qSort(scripDetailsList);

        qDebug()<<scripDetailsList.size();
        for(int j = 0; j < scripDetailsList.size(); j++)
        {
            ScripInfo scrip = scripDetailsList[j];

            qDebug()<<scripNames[i] + "-" + seriesMap.value(j+1, QString().fill('I', j+1)) + ",";

            QString scripData = scripNames[i] + "-" + seriesMap.value(j+1, QString().fill('I', j+1)) + ","
                                + dateString + ","
                                + scrip.open + ","
                                + scrip.high + ","
                                + scrip.low + ","
                                + scrip.close + ","

                                + scrip.volume + ","
                                + scrip.openInterest + "\n";

            out << scripData;
            if(!option)
                consolidatedOutput <<scripData;

            /*out << scripNames[i] + "-" + seriesMap.value(j+1, QString().fill('I', j+1)) + ",";
            out << dateString + ",";
            out << scrip.open + ",";
            out << scrip.high + ",";
            out << scrip.low + ",";
            out << scrip.close + ",";

            out << scrip.volume + ",";
            out << scrip.openInterest + "\n";*/
        }
    }

    file.close();
}


bool ScripInfo::operator== ( const ScripInfo & other ) const
{
    return other.expiryDate == this->expiryDate;
}

bool ScripInfo::operator< ( const ScripInfo & other ) const
{
    return this->expiryDate < other.expiryDate;
}
